package com.easipos.temptrack.bundle

object ParcelData {

    const val CLEAR_DB = "clear_db"
    const val URL = "url"
    const val QR_CONTENT = "QR_CONTENT"
}