package com.easipos.temptrack.api.services

import com.easipos.temptrack.api.ApiEndpoint
import com.easipos.temptrack.api.misc.EmptyResponseModel
import com.easipos.temptrack.api.misc.ResponseModel
import com.easipos.temptrack.api.requests.temp.SubmitEntranceRecordRequestModel
import io.reactivex.Single
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.POST

interface Api {

    @POST(ApiEndpoint.CHECK_VERSION)
    fun checkVersion(@Body body: RequestBody): Single<ResponseModel<Boolean>>

    @POST(ApiEndpoint.SUBMIT_ENTRANCE_RECORD)
    fun submitEntranceRecord(@Body body: SubmitEntranceRecordRequestModel): Single<EmptyResponseModel>
}
