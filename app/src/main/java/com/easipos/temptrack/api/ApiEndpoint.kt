package com.easipos.temptrack.api

object ApiEndpoint {

    const val CHECK_VERSION = "ddcard/version/check"

    const val SUBMIT_ENTRANCE_RECORD = "entranceRecord"
}
