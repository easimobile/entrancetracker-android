package com.easipos.temptrack.api.requests.temp

import com.google.gson.annotations.SerializedName

data class SubmitEntranceRecordRequestModel(

    @SerializedName("qrContent")
    val qrContent: String,

    @SerializedName("outletCode")
    val outletCode: String,

    @SerializedName("temperature")
    val temperature: Double
)