package com.easipos.temptrack.event_bus

data class NotificationCount(val count: Int)