package com.easipos.temptrack.repositories.precheck

import com.easipos.temptrack.api.requests.precheck.CheckVersionRequestModel
import com.easipos.temptrack.datasource.DataFactory
import io.reactivex.Single

class PrecheckDataRepository(private val dataFactory: DataFactory) : PrecheckRepository {

    override fun checkVersion(model: CheckVersionRequestModel): Single<Boolean> =
        dataFactory.createPrecheckDataSource()
            .checkVersion(model)
}
