package com.easipos.temptrack.repositories.temp

import com.easipos.temptrack.api.requests.temp.SubmitEntranceRecordRequestModel
import com.easipos.temptrack.datasource.DataFactory
import io.reactivex.Observable

class TempDataRepository(private val dataFactory: DataFactory) : TempRepository {

    override fun submitEntranceRecord(model: SubmitEntranceRecordRequestModel): Observable<Void> =
        dataFactory.createTempDataSource()
            .submitEntranceRecord(model)
}
