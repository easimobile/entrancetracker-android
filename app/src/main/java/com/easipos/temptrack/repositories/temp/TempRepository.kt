package com.easipos.temptrack.repositories.temp

import com.easipos.temptrack.api.requests.temp.SubmitEntranceRecordRequestModel
import io.reactivex.Observable

interface TempRepository {

    fun submitEntranceRecord(model: SubmitEntranceRecordRequestModel): Observable<Void>
}
