package com.easipos.temptrack.repositories.precheck

import com.easipos.temptrack.api.requests.precheck.CheckVersionRequestModel
import io.reactivex.Single

interface PrecheckRepository {

    fun checkVersion(model: CheckVersionRequestModel): Single<Boolean>
}
