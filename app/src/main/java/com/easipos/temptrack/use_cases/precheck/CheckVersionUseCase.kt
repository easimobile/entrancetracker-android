package com.easipos.temptrack.use_cases.precheck

import com.easipos.temptrack.api.requests.precheck.CheckVersionRequestModel
import com.easipos.temptrack.repositories.precheck.PrecheckRepository
import com.easipos.temptrack.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class CheckVersionUseCase(kodein: Kodein)
    : AbsRxSingleUseCase<Boolean, CheckVersionUseCase.Params>(kodein) {

    private val repository: PrecheckRepository by kodein.instance()

    override fun createSingle(params: Params): Single<Boolean> =
        repository.checkVersion(params.model)

    class Params private constructor(val model: CheckVersionRequestModel) {
        companion object {
            fun createQuery(model: CheckVersionRequestModel): Params {
                return Params(model)
            }
        }
    }
}
