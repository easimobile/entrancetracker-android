package com.easipos.temptrack.use_cases.temp

import com.easipos.temptrack.api.requests.temp.SubmitEntranceRecordRequestModel
import com.easipos.temptrack.repositories.temp.TempRepository
import com.easipos.temptrack.use_cases.base.AbsRxObservableUseCase
import io.reactivex.Observable
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class SubmitEntranceRecordUseCase(kodein: Kodein)
    : AbsRxObservableUseCase<Void, SubmitEntranceRecordUseCase.Params>(kodein) {

    private val repository by kodein.instance<TempRepository>()

    override fun createObservable(params: Params): Observable<Void> =
        repository.submitEntranceRecord(params.model)

    class Params private constructor(val model: SubmitEntranceRecordRequestModel) {
        companion object {
            fun createQuery(model: SubmitEntranceRecordRequestModel): Params {
                return Params(model)
            }
        }
    }
}
