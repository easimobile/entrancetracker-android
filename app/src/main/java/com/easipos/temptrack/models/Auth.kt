package com.easipos.temptrack.models

data class Auth(val token: String)