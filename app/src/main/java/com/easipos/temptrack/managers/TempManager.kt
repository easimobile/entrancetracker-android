package com.easipos.temptrack.managers

import android.content.Context
import com.idatachina.imeasuresdk.IMeasureSDK
import com.orhanobut.logger.Logger

object TempManager {

    private var mIMeasureSDK: IMeasureSDK? = null

    private var isScanning = false

    fun init(context: Context) {
        mIMeasureSDK = IMeasureSDK(context)
        mIMeasureSDK?.init(object : IMeasureSDK.InitCallback {
            override fun failed(p0: Int, p1: String?) {
                Logger.d("InitCallback: $p0, $p1")
            }

            override fun disconnect() {
                Logger.d("Temp Terminal Disconnected")
            }

            override fun success() {
                Logger.d("Temp Terminal Connected")
            }
        })
    }

    fun scanTemp(successCallback: (Double) -> Unit, failedCallback: (Int, String?) -> Unit) {
        if (isReady() && isScanning.not()) {
            Logger.d("Temp scanning")
            isScanning = true
            mIMeasureSDK?.read(object : IMeasureSDK.TemperatureCallback {
                override fun failed(p0: Int, p1: String?) {
                    Logger.d("TemperatureCallback: $p0, $p1")
                    isScanning = false
                    failedCallback(p0, p1)
                }

                override fun success(p0: Double) {
                    Logger.d("Temp: $p0")
                    isScanning = false
                    successCallback(p0)
                }
            })
        }
    }

    fun disconnect() {
        if (isReady()) {
            mIMeasureSDK?.close()
        }
    }

    fun reconnect() {
        if (isReady()) {
            mIMeasureSDK?.reconect()
        }
    }

    private fun isReady(): Boolean {
        return mIMeasureSDK != null && mIMeasureSDK!!.isReady
    }
}