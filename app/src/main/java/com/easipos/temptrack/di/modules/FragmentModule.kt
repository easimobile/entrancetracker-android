package com.easipos.temptrack.di.modules

import org.kodein.di.Kodein

fun provideFragmentModule() = Kodein.Module("fragmentModule") {
}
