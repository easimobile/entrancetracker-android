package com.easipos.temptrack.di.modules

import com.easipos.temptrack.activities.main.navigation.MainNavigation
import com.easipos.temptrack.activities.main.navigation.MainNavigationImpl
import com.easipos.temptrack.activities.settings.navigation.SettingsNavigation
import com.easipos.temptrack.activities.settings.navigation.SettingsNavigationImpl
import com.easipos.temptrack.activities.splash.navigation.SplashNavigation
import com.easipos.temptrack.activities.splash.navigation.SplashNavigationImpl
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.provider

fun provideActivityModule() = Kodein.Module("activityModule") {
    bind<SplashNavigation>() with provider { SplashNavigationImpl() }
    bind<MainNavigation>() with provider { MainNavigationImpl() }
    bind<SettingsNavigation>() with provider { SettingsNavigationImpl() }
}
