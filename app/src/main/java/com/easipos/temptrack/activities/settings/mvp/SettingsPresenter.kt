package com.easipos.temptrack.activities.settings.mvp

import android.app.Application
import com.easipos.temptrack.base.Presenter

class SettingsPresenter(application: Application)
    : Presenter<SettingsView>(application)
