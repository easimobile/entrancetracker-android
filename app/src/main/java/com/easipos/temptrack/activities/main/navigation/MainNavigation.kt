package com.easipos.temptrack.activities.main.navigation

import android.app.Activity

interface MainNavigation {

    fun navigateToSettings(activity: Activity)
}