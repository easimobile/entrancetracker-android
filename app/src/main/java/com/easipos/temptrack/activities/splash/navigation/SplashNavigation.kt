package com.easipos.temptrack.activities.splash.navigation

import android.app.Activity

interface SplashNavigation {

    fun navigateToMain(activity: Activity)
}