package com.easipos.temptrack.activities.main.mvp

import android.app.Application
import com.easipos.temptrack.R
import com.easipos.temptrack.api.requests.temp.SubmitEntranceRecordRequestModel
import com.easipos.temptrack.base.Presenter
import com.easipos.temptrack.use_cases.base.DefaultObserver
import com.easipos.temptrack.use_cases.temp.SubmitEntranceRecordUseCase
import com.easipos.temptrack.util.ErrorUtil

class MainPresenter(application: Application)
    : Presenter<MainView>(application) {

    private val submitEntranceRecordUseCase by lazy { SubmitEntranceRecordUseCase(kodein) }

    override fun onDetachView() {
        super.onDetachView()
        submitEntranceRecordUseCase.dispose()
    }

    fun doSubmitEntranceRecord(model: SubmitEntranceRecordRequestModel) {
        view?.setLoadingIndicator(true)
        submitEntranceRecordUseCase.execute(object : DefaultObserver<Void>() {
            override fun onComplete() {
                view?.setLoadingIndicator(false)
                view?.showErrorAlertDialog(application.getString(R.string.prompt_entrance_record_submit_successfully)) {
                    view?.dismissDialog()
                    view?.resumeCameraPreview()
                }
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                view?.setLoadingIndicator(false)
                view?.showErrorAlertDialog(ErrorUtil.parseException(error))
            }
        }, SubmitEntranceRecordUseCase.Params.createQuery(model))
    }
}
