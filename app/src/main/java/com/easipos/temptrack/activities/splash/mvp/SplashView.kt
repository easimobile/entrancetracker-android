package com.easipos.temptrack.activities.splash.mvp

import com.easipos.temptrack.base.View

interface SplashView : View {

    fun showUpdateAppDialog()

    fun navigateToLogin()

    fun navigateToMain()
}
