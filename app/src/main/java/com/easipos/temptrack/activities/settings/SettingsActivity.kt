package com.easipos.temptrack.activities.settings

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.easipos.temptrack.R
import com.easipos.temptrack.activities.settings.mvp.SettingsPresenter
import com.easipos.temptrack.activities.settings.mvp.SettingsView
import com.easipos.temptrack.activities.settings.navigation.SettingsNavigation
import com.easipos.temptrack.base.CustomBaseAppCompatActivity
import com.easipos.temptrack.fragments.alert_dialog.CommonAlertDialog
import com.easipos.temptrack.tools.Preference
import io.github.anderscheow.library.kotlinExt.click
import kotlinx.android.synthetic.main.activity_settings.*
import org.jetbrains.anko.longToast
import org.jetbrains.anko.selector
import org.kodein.di.generic.instance

class SettingsActivity : CustomBaseAppCompatActivity(), SettingsView {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, SettingsActivity::class.java)
        }
    }

    //region Variables
    private val navigation by instance<SettingsNavigation>()

    private val presenter by lazy { SettingsPresenter(application) }

    private var selectedOutlet: String? = null
    //endregion

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_settings

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        setupViews()
        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog.show(this, message, action)
    }
    //endregion

    //region Interface Methods
    //endregion

    //region Action Methods
    private fun setupViews() {
        selectedOutlet = Preference.prefOutletCode

        text_view_outlet.text = selectedOutlet
        text_input_edit_text_low_temperature.setText(Preference.prefTempLow.toString())
        text_input_edit_text_high_temperature.setText(Preference.prefTempHigh.toString())
    }

    private fun setupListeners() {
        image_view_back.click {
            if (validateFields()) {
                finish()
            }
        }

        text_view_save.click {
            attemptSaveSettings()
        }

        card_view_outlet.click {
            val outlets = arrayListOf<String>().apply {
                this.add("Outlet A")
                this.add("Outlet B")
                this.add("Outlet C")
            }

            selector(
                title = getString(R.string.label_outlet),
                items = outlets,
                onClick = { _, index ->
                    selectedOutlet = outlets[index]
                    text_view_outlet.text = selectedOutlet
                }
            )
        }
    }

    private fun attemptSaveSettings() {
        if (validateFields()) {
            Preference.prefOutletCode = selectedOutlet!!
            Preference.prefTempLow = text_input_edit_text_low_temperature.text?.toString()?.toDoubleOrNull() ?: 0.0
            Preference.prefTempHigh = text_input_edit_text_high_temperature.text?.toString()?.toDoubleOrNull() ?: 0.0

            showErrorAlertDialog(getString(R.string.prompt_settings_save_successfully)) {
                finish()
            }
        }
    }

    private fun validateFields(): Boolean {
        if (selectedOutlet == null || selectedOutlet!!.isBlank()) {
            showErrorAlertDialog(getString(R.string.error_outlet_required))
            return false
        }

        val lowTempInText = text_input_edit_text_low_temperature.text?.toString() ?: ""
        val highTempInText = text_input_edit_text_high_temperature.text?.toString() ?: ""

        if (lowTempInText.isBlank()) {
            showErrorAlertDialog(getString(R.string.error_low_temp_required))
            return false
        }

        if (highTempInText.isBlank()) {
            showErrorAlertDialog(getString(R.string.error_high_temp_required))
            return false
        }

        val lowTemp = lowTempInText.toDoubleOrNull() ?: 0.0
        val highTemp = highTempInText.toDoubleOrNull() ?: 0.0

        if (lowTemp > highTemp) {
            showErrorAlertDialog(getString(R.string.error_low_temp_larger_than_high_temp))
            return false
        }

        return true
    }
    //endregion
}
