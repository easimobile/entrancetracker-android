package com.easipos.temptrack.activities.main

import android.Manifest
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.AudioManager
import android.media.ToneGenerator
import android.os.Bundle
import com.easipos.temptrack.R
import com.easipos.temptrack.activities.main.mvp.MainPresenter
import com.easipos.temptrack.activities.main.mvp.MainView
import com.easipos.temptrack.activities.main.navigation.MainNavigation
import com.easipos.temptrack.api.requests.temp.SubmitEntranceRecordRequestModel
import com.easipos.temptrack.base.CustomBaseAppCompatActivity
import com.easipos.temptrack.fragments.alert_dialog.CommonAlertDialog
import com.easipos.temptrack.fragments.dialog_fragment.ScanTempDialogFragment
import com.easipos.temptrack.managers.TempManager
import com.easipos.temptrack.tools.Preference
import com.google.zxing.Result
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.orhanobut.logger.Logger
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.findColor
import kotlinx.android.synthetic.main.activity_main.*
import me.dm7.barcodescanner.zxing.ZXingScannerView
import org.jetbrains.anko.longToast
import org.kodein.di.generic.instance

class MainActivity : CustomBaseAppCompatActivity(), MainView, ZXingScannerView.ResultHandler {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    //region Variables
    private val navigation by instance<MainNavigation>()

    private val presenter by lazy { MainPresenter(application) }

    private val mScannerView by lazy { findViewById<ZXingScannerView>(R.id.layout_barcode) }

    private val toneGenerator by lazy { ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100) }
    private var scanTempDialogFragment: ScanTempDialogFragment? = null
    //endregion

    override fun onStart() {
        super.onStart()
        TempManager.reconnect()
    }

    override fun onResume() {
        super.onResume()
        mScannerView.setResultHandler(this)
        attemptStartCamera()
    }

    override fun onPause() {
        mScannerView.stopCamera()
        super.onPause()
    }

    override fun onDestroy() {
        TempManager.disconnect()
        toneGenerator.release()
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_main

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        TempManager.init(baseContext)

        setupViews()
        setupListeners()

        if (Preference.prefOutletCode.isBlank() || Preference.prefTempLow == 0.0 || Preference.prefTempHigh == 0.0) {
            navigation.navigateToSettings(this)
        }
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog.show(this, message, action)
    }

    override fun resumeCameraPreview() {
        runOnUiThread {
            mScannerView.startCamera()
            mScannerView.setResultHandler(this)
        }
    }

    override fun dismissDialog() {
        scanTempDialogFragment?.dismissAllowingStateLoss()
    }
    //endregion

    //region Interface Methods
    override fun handleResult(rawResult: Result?) {
        rawResult?.let { result ->
            Logger.d(result)
            toneGenerator.startTone(ToneGenerator.TONE_PROP_BEEP)
            scanTempDialogFragment = ScanTempDialogFragment.show(this, result.text)
        }
    }
    //endregion

    //region Action Methods
    fun submitEntranceRecord(temp: Double, qrContent: String) {
        val model = SubmitEntranceRecordRequestModel(
            qrContent = qrContent,
            outletCode = Preference.prefOutletCode,
            temperature = temp
        )

        presenter.doSubmitEntranceRecord(model)
    }

    private fun setupViews() {
        mScannerView.apply {
            this.setBorderCornerRadius(4)
            this.setIsBorderCornerRounded(true)
            this.setBorderColor(findColor(R.color.colorAccent))
            this.setMaskColor(Color.parseColor("#66000000"))
        }
    }

    private fun setupListeners() {
        image_view_settings.click {
            navigation.navigateToSettings(this)
        }
    }

    private fun attemptStartCamera() {
        Dexter.withActivity(this)
            .withPermission(Manifest.permission.CAMERA)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                    mScannerView.startCamera()
                }

                override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest?, token: PermissionToken?) {
                    Logger.d("onPermissionRationaleShouldBeShown")
                    token?.continuePermissionRequest()
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                    showErrorAlertDialog(getString(R.string.error_permission_camera)) {
                        finish()
                    }
                }
            }).check()
    }
    //endregion
}
