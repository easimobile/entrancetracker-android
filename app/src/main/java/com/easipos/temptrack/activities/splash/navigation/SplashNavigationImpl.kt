package com.easipos.temptrack.activities.splash.navigation

import android.app.Activity
import com.easipos.temptrack.activities.main.MainActivity

class SplashNavigationImpl : SplashNavigation {

    override fun navigateToMain(activity: Activity) {
        activity.startActivity(MainActivity.newIntent(activity))
        activity.finishAffinity()
    }
}