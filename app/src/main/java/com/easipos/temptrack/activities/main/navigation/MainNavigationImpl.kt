package com.easipos.temptrack.activities.main.navigation

import android.app.Activity
import com.easipos.temptrack.activities.settings.SettingsActivity

class MainNavigationImpl :  MainNavigation {

    override fun navigateToSettings(activity: Activity) {
        activity.startActivity(SettingsActivity.newIntent(activity))
    }
}