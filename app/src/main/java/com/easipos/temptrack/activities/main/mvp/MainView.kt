package com.easipos.temptrack.activities.main.mvp

import com.easipos.temptrack.base.View

interface MainView : View {

    fun resumeCameraPreview()

    fun dismissDialog()
}
