package com.easipos.temptrack.datasource

import android.app.Application
import com.easipos.temptrack.api.services.Api
import com.easipos.temptrack.datasource.precheck.PrecheckDataSource
import com.easipos.temptrack.datasource.precheck.PrecheckDataStore
import com.easipos.temptrack.datasource.temp.TempDataSource
import com.easipos.temptrack.datasource.temp.TempDataStore
import com.easipos.temptrack.executor.PostExecutionThread
import com.easipos.temptrack.executor.ThreadExecutor
import com.easipos.temptrack.room.RoomService

class DataFactory(private val application: Application,
                  private val api: Api,
                  private val roomService: RoomService,
                  private val threadExecutor: ThreadExecutor,
                  private val postExecutionThread: PostExecutionThread) {

    fun createPrecheckDataSource(): PrecheckDataStore =
        PrecheckDataSource(api, threadExecutor, postExecutionThread)

    fun createTempDataSource(): TempDataStore =
        TempDataSource(api, threadExecutor, postExecutionThread)
}
