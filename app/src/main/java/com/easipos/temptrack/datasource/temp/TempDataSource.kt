package com.easipos.temptrack.datasource.temp

import com.easipos.temptrack.api.misc.EmptySingleObserverRetrofit
import com.easipos.temptrack.api.requests.temp.SubmitEntranceRecordRequestModel
import com.easipos.temptrack.api.services.Api
import com.easipos.temptrack.executor.PostExecutionThread
import com.easipos.temptrack.executor.ThreadExecutor
import com.easipos.temptrack.use_cases.complete
import com.easipos.temptrack.use_cases.error
import com.orhanobut.logger.Logger
import io.reactivex.Observable

class TempDataSource(private val api: Api,
                     private val threadExecutor: ThreadExecutor,
                     private val postExecutionThread: PostExecutionThread) : TempDataStore {

    override fun submitEntranceRecord(model: SubmitEntranceRecordRequestModel): Observable<Void> =
        Observable.create { emitter ->
            api.submitEntranceRecord(model)
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : EmptySingleObserverRetrofit() {
                    override fun onResponseSuccess() {
                        Logger.i("Entrance record submitted")

                        emitter.complete()
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }
}
