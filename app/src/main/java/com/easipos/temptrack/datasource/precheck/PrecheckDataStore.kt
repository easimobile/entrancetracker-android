package com.easipos.temptrack.datasource.precheck

import com.easipos.temptrack.api.requests.precheck.CheckVersionRequestModel
import io.reactivex.Single

interface PrecheckDataStore {

    fun checkVersion(model: CheckVersionRequestModel): Single<Boolean>
}
