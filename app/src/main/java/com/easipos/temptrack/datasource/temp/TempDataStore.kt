package com.easipos.temptrack.datasource.temp

import com.easipos.temptrack.api.requests.temp.SubmitEntranceRecordRequestModel
import io.reactivex.Observable

interface TempDataStore {

    fun submitEntranceRecord(model: SubmitEntranceRecordRequestModel): Observable<Void>
}
