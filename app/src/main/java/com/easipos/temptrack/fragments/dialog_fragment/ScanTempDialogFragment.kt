package com.easipos.temptrack.fragments.dialog_fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.easipos.temptrack.R
import com.easipos.temptrack.activities.main.MainActivity
import com.easipos.temptrack.base.CustomBaseDialogFragment
import com.easipos.temptrack.bundle.ParcelData
import com.easipos.temptrack.managers.TempManager
import com.easipos.temptrack.tools.Preference
import com.orhanobut.logger.Logger
import io.github.anderscheow.library.appCompat.activity.FoundationAppCompatActivity
import io.github.anderscheow.library.kotlinExt.*
import kotlinx.android.synthetic.main.dialog_fragment_scan_temp.*

class ScanTempDialogFragment : CustomBaseDialogFragment() {

    companion object {
        fun show(activity: FoundationAppCompatActivity, qrContent: String): ScanTempDialogFragment {
            val fragment = ScanTempDialogFragment().apply {
                this.arguments = Bundle().apply {
                    this.putString(ParcelData.QR_CONTENT, qrContent)
                }
            }

            activity.removeDialogFragmentThen(ScanTempDialogFragment.TAG) {
                fragment
            }

            return fragment
        }
    }

    private val qrContent by argument<String>(ParcelData.QR_CONTENT)

    private var userTemp: Double = 0.0

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog?.setOnKeyListener { _, keyCode, _ ->
            Logger.d(keyCode)
            if (keyCode == 600 || keyCode == 601 || keyCode == 602) {
                scanTemp()
            }
            true
        }
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onDestroyView() {
        dialog?.setOnKeyListener(null)
        super.onDestroyView()
    }

    override fun getResLayout(): Int = R.layout.dialog_fragment_scan_temp

    override fun init() {
        super.init()

        button_submit.gone()

        button_cancel.click {
            (activity as? MainActivity)?.resumeCameraPreview()
            dismissAllowingStateLoss()
        }

        button_submit.click {
            if (userTemp > 0.0 && qrContent != null) {
                (activity as? MainActivity)?.submitEntranceRecord(userTemp, qrContent!!)
            }
        }
    }

    private fun scanTemp() {
        TempManager.scanTemp(
            successCallback = { temp ->
                activity?.runOnUiThread {
                    withContext { context ->
                        button_submit.visible()
                        text_view_scan_temperature.gone()
                        text_view_temperature.visible()

                        val newTempInString = String.format("%.2f", temp)
                        userTemp = newTempInString.toDoubleOrNull() ?: 0.0

                        text_view_temperature.text = newTempInString

                        if (Preference.prefTempHigh > 0) {
                            when {
                                userTemp <= Preference.prefTempLow -> {
                                    text_view_temperature.setBackgroundColor(context.findColor(R.color.colorLow))
                                }
                                userTemp >= Preference.prefTempHigh -> {
                                    text_view_temperature.setBackgroundColor(context.findColor(R.color.colorHigh))
                                }
                                else -> {
                                    text_view_temperature.setBackgroundColor(context.findColor(R.color.colorMedium))
                                }
                            }
                        } else {
                            if (userTemp <= Preference.prefTempLow) {
                                text_view_temperature.setBackgroundColor(context.findColor(R.color.colorLow))
                            } else {
                                text_view_temperature.setBackgroundColor(context.findColor(R.color.colorHigh))
                            }
                        }
                    }
                }
            },
            failedCallback = { code, string ->
                activity?.runOnUiThread {
                    button_submit.gone()
                    text_view_scan_temperature.visible()
                    text_view_temperature.gone()

                    (activity as? MainActivity)?.showErrorAlertDialog("$code: $string")
                }
            }
        )
    }
}